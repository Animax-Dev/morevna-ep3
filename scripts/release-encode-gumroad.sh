#!/bin/bash

SCRIPTPATH=$(cd `dirname "$0"`; pwd)

SOURCE="$1"
OUTPUT="$2"
if [ ! -z "$3" ]; then
DURATION="-t 00:10:06"
fi

ffmpeg -y -i "${SOURCE}" ${DURATION} -c:v libx264 -preset slow -b:v 4000k -x264-params keyint=5 -c:a aac -b:a 224k -pass 1 -f mp4 /dev/null && \
ffmpeg -y -i "${SOURCE}"  ${DURATION} -c:v libx264 -preset slow -b:v 4000k -x264-params keyint=5 -c:a aac -b:a 224k -pass 2 "${OUTPUT}"
