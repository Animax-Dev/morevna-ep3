#!/bin/sh

# This script is used to create dub-pack - it extracts audio tracks
# from blender file and encodes them to FLAC.
# Read more about dub pack here - 
# https://morevnaproject.org/2016/08/22/call-for-fandub/

PREFIX=`dirname $0`
BLENDFILE="${PREFIX}/../ep03-en.blend"

cd ${PREFIX}/..

set -e

renderchan --recursive ${PREFIX}/../sound.en || true
renderchan --recursive ${PREFIX}/../sound.ru || true
renderchan --recursive ${PREFIX}/../sound-fx || true

# Voice
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 18
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 19
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 20
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 21

# Music
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 12 13

# FX
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 7
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 8
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 9
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 10
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 14
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 15
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 16
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 17

BLENDFILE="${PREFIX}/../ep03-ru.blend"

bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 22
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 23
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 24


